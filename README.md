# sis-header

A header view component for Table and Charts.

## setup

- install: `npm i sis-header` (with the nexus configuration)
- live: `./node_modules/web-component-env/wce start`

## usage

You can pass other components as children to be rendered inside this component
```javascript
import Header from 'sis-header';
import Share from 'oecd-share';

<div>
  <Header
    subtitle={subtitle}
    title={title}
    toggleInfoRequest={null}
    uprs={uprs}
  >
    <Share {...shareData} />
  </Header>
</div>
```

## api

#### title `<string>`
```javascript
'Key Short-Term Economic Indicators'
```

#### subtitle `<string>`
```javascript
'Measure: Growth previous period - Frequency: Annual - Time: 2015'
```

#### uprs `<object>` subset of data attributes
```javascript
{
  U: {
    "id": "UNIT",
    "name": "Unit",
    "values": [
      {
        "id": "PC",
        "name": "Percentage"
      }
    ],
    role": "UNIT_MEASURE"
  },
  P: { ... },
  R: { ... }
}
```
note: if any of this attributes has more than one value, only the first one will be considered

#### toggleInfoRequest `<function>`
Optional parameter, if present, component will render an information icon next to the title, who will
call this function on click.