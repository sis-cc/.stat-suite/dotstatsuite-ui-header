import React, { Component } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes } from '@blueprintjs/core';
import { isNumber, reduce, size, uniqueId } from 'lodash';

const fontSizeByWidth = (width) => {
  if (!isNumber(width))
    return 16;
  if (width < 250)
    return 0;
  else if (width <= 270)
    return 12;
  else if (width <= 760)
    return 13;
  else
    return 16;
};

const Container = glamorous.div({
  alignItems: 'center',
  color: '#7A7A7A',
  display: 'flex',
  flexWrap: 'wrap',
  fontFamily: 'bernino-sans-narrow-regular',
  fontSize: 16,
  width: '100%' //IE
}, ({ fonts={}, width }) => ({
  ...fonts,
  fontSize: fontSizeByWidth(width)
}));

const Separator = glamorous.span({
  color: '#0297C9',
  fontSize: '10px !important',
  margin: 5
});

const Entry = glamorous.div({
  flexDirection: 'row'
})

const Subtitle = ({ fonts, entries, width }) => {
  const nEntries = size(entries);

  const entriesToDOM = reduce(
    entries,
    (memo, entry, index) => {
      memo.push(
        <Entry
          key={uniqueId('entry')}
        >
          {entry}
        </Entry>
      );
      if (index < nEntries - 1) {
        memo.push(
          <Separator
            className={`${Classes.ICON_STANDARD} ${Classes.iconClass('symbol-circle')}`}
            key={uniqueId('separator')}
          />
        );
      }
      return memo;
    },
    []
  );

  return (
    <Container
      fonts={fonts}
      width={width}
    >
      { entriesToDOM }
    </Container>
  );
};

Subtitle.propTypes = {
  fonts: PropTypes.object,
  entries: PropTypes.arrayOf(PropTypes.string),
  width: PropTypes.number
};

export default Subtitle;
