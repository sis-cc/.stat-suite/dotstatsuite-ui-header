import glamorous from 'glamorous';
import { isNumber } from 'lodash';

const fontSizeByWidth = (width) => {
  if (!isNumber(width))
    return 16;
  if (width < 250)
    return 0;
  else if (width <= 270)
    return 12;
  else if (width <= 760)
    return 13;
  else
    return 16;
};

export default glamorous.div({
  alignItems: 'center',
  color: '#7A7A7A',
  fontFamily: 'bernino-sans-narrow-regular',
  fontSize: 16,
  width: '100%' //IE
},
({ fonts={}, width }) => ({
  ...fonts,
  fontSize: fontSizeByWidth(width),
}));
