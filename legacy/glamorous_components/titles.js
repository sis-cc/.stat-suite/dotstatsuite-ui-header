import glamorous from 'glamorous';

export default glamorous.div({
  alignItems: 'baseline',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  marginBottom: 10,
  marginTop: 10,
});
