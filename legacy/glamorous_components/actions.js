import glamorous from 'glamorous';

export default glamorous.div({
  alignItems: 'center',
  display: 'flex',
});