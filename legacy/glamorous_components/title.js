import React, { Component } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes } from '@blueprintjs/core';
import { isFunction, isNumber } from 'lodash';

const fontSizeByWidth = (width) => {
  if (!isNumber(width))
    return 24;
  if (width < 250)
    return 0;
  if (width <= 270)
    return 12;
  if (width <= 370)
    return 14;
  if (width <= 560)
    return 16;
  if (width <= 760)
    return 18;
  if (width <= 855)
    return 20;
  if (width <= 1140)
    return 22; 
  return 24;
}

const Container = glamorous.div({
  color: '#0297C9',
  fontFamily: 'caecilia',
  width: '100%' //IE
},
({ fonts={}, width }) => {
  return ({
  ...fonts,
  fontSize: fontSizeByWidth(width)
  });
});

const Icon = glamorous.span({
  cursor: 'pointer',
  marginLeft: 10,
  position: 'relative',
  top: -2,
});

const Title = ({ clickCallback, entry, fonts, width }) => {
  return (
    <Container
      width={width}
      fonts={fonts}
    >
      <span>{ entry }</span>
      {
        isFunction(clickCallback)
        ? <Icon
            className={`${Classes.ICON_STANDARD} ${Classes.iconClass('info-sign')}`}
            onClick={e => { clickCallback() }}
          />
          : null
      }
    </Container>
  );
};

Title.propTypes = {
  entry: PropTypes.string,
  fonts: PropTypes.object,
  clickCallback: PropTypes.func,
  width: PropTypes.number,
};

export default Title;
