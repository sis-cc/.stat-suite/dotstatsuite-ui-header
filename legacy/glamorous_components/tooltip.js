import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Intent, Position, Popover, PopoverInteractionKind } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { css } from 'glamor';
import { map } from 'lodash';

export const Ul = glamorous.ul({
  backgroundColor: '#30404D',
  borderRadius: 3,
  boxSizing: 'border-box',
  color: 'white',
  fontFamily: 'bernino-sans-narrow-regular',
  fontSize: 12,
  listStyleType: 'none',
  padding: 10
}, ({ fonts={} }) => ({
  ...fonts
}));

const PopoverStyle = css({
  boxShadow: '0px 0px white !important',
  WebkitBoxShadow: '0px 0px white !important',
});

const Icon = glamorous.span({
  color: '#5c7080',
  cursor: 'pointer',
  marginLeft: 10,
  position: 'relative',
  top: -2,
});

const Tooltip = ({ entries, fonts }) => {
 const content = 
    <Ul fonts={fonts}>
      {
        map(
          entries,
          (entry, index) => <li key={index}>{entry}</li>
        )
      }
    </Ul>;

  return (
    <Popover
      content={content}
      hoverCloseDelay={50}
      hoverOpenDelay={0}
      interactionKind={PopoverInteractionKind.HOVER}
      openOnTargetFocus={true}
      popoverClassName={`${Classes.MINIMAL} ${PopoverStyle}`}
      position={Position.BOTTOM}
      tetherOptions={{
        constraints: [
          { to: 'scrollParent', pin: true },
          { attachment: "together", to: "scrollParent" }
        ]
      }}
    >
      <Icon
        className={`${Classes.ICON_STANDARD} ${Classes.iconClass('info-sign')}`}
      />
    </Popover>
  );
};

Tooltip.propTypes = {
  entries: PropTypes.arrayOf(PropTypes.string),
  fonts: PropTypes.object,
};

export default Tooltip;