import React from 'react';
import { render } from 'react-dom';
import { Button } from '@blueprintjs/core';
import Header from '.';
import './assets/app.less';

const title = 'Key Short-Term Economic Indicators';
const subtitle = [
  'Measure: Growth previous period',
  'Frequency: Annual', 'Time: 2015',
  'Banknote & coin related items: Collector coins - Collector coins issued less collector coins returned; not intended for circulation; (net issuance)',
  'BKN denomination breakdown: All denominations',
  'Banknote/coin series: Not applicable',
  'Series denominat/spec calcul: National currency',
];
const uprs = 'Percentage';
const toggleInfoRequest = () => console.log('toggle !');

const fonts = {
  subtitle: {
    color: 'red',
    fontFamily: 'Comic sans MS'
  },
  title: {
    color: 'green',
    fontFamily: 'Comic sans MS',
  },
  uprs: {
    color: 'purple',
    fontFamily: 'Comic sans MS'
  },
  tooltip: {
    fontFamily: 'Comic sans MS',
    backgroundColor: 'yellow',
    color: 'blue'
  }
}

const App = () => (
  <div>
    <div style={{ width: 200 }}>
      <span className="app-note">200</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={200} />
    </div>
    <div style={{ width: 260 }}>
      <span className="app-note">260</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={260} />
    </div>
    <div style={{ width: 360 }}>
      <span className="app-note">360</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={360} />
    </div>
    <div style={{ width: 550 }}>
      <span className="app-note">550</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={550} />
    </div>
    <div style={{ width: 750 }}>
      <span className="app-note">750</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={750} />
    </div>
    <div style={{ width: 850 }}>
      <span className="app-note">850</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={850} />
    </div>
    <div style={{ width: 1130 }}>
      <span className="app-note">1130</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={1130} />
    </div>
    <div style={{ width: 1200 }}>
      <span className="app-note">1200</span>
      <Header uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={1200} />
    </div>
    <div style={{ width: 260 }}>
      <span className="app-note">260 Comic sans MS</span>
      <Header fonts={fonts} uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={260} />
    </div>
    <div style={{ width: 1200 }}>
      <span className="app-note">1200 Comic sans MS</span>
      <Header fonts={fonts} uprs={uprs} subtitle={subtitle} title={title} toggleInfoRequest={toggleInfoRequest} width={1200} />
    </div>
  </div>
);

render(<App />, document.getElementById('root'));