import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { get, isNumber, reduce, size } from 'lodash';
import '@blueprintjs/core/dist/blueprint.css';
import './assets/header.less';
import {
  Actions,
  Container,
  Subtitle,
  Title,
  Titles,
  Tooltip,
  Uprs
} from './glamorous_components';

const Header = ({ children, fonts, subtitle, title, toggleInfoRequest, uprs, width }) => {

  return (
    <Container>
      <Titles>
        <Title
          clickCallback={toggleInfoRequest}
          entry={title}
          fonts={get(fonts, 'title', {})}
          width={width}
        />
        <Subtitle
          entries={subtitle}
          fonts={get(fonts, 'subtitle', {})}
          width={width}
        />
        <Uprs fonts={get(fonts, 'uprs', {})} width={width}>
          { uprs }
        </Uprs>
      </Titles>
      <Actions>
        { children }
      </Actions>
    </Container>
  );
}

Header.propTypes = {
  fonts: PropTypes.object,
  subtitle: PropTypes.array,
  title: PropTypes.string,
  toggleInfoRequest: PropTypes.func,
  uprs: PropTypes.string,
};

const ReducingTooltipHeader = ({ fonts, width=NaN, children, title, subtitle, uprs, toggleInfoRequest }) => {

  const subtitleLength = reduce(
    subtitle,
    (memo, entry) => memo + size(entry),
    0
  );

  if (isNumber(width) && width < 250) {
    return (
      <Header>
        <Tooltip entries={[title, ...subtitle, uprs]} fonts={get(fonts, 'tooltip', {})} />
        {children}
      </Header>   
    );
  }
  else if (isNumber(width) && width <= 270 && subtitleLength > 40) {
    return (
      <Header title={title} uprs={uprs} width={width} fonts={fonts}>
        <Tooltip entries={subtitle} fonts={get(fonts, 'tooltip', {})} />
        {children}
      </Header>   
    );
  }
  return (
    <Header
      fonts={fonts}
      title={title}
      subtitle={subtitle}
      uprs={uprs}
      toggleInfoRequest={toggleInfoRequest}
      width={width}
    >
      {children}
    </Header>
  );
}

ReducingTooltipHeader.propTypes = {
  fonts: PropTypes.object,
  subtitle: PropTypes.array,
  title: PropTypes.string,
  toggleInfoRequest: PropTypes.func,
  uprs: PropTypes.string,
  width: PropTypes.number
};

export default ReducingTooltipHeader;