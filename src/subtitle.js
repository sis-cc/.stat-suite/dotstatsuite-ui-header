import React, { Component } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Classes } from '@blueprintjs/core';
import * as R from 'ramda';
import Item from './item';

const Container = glamorous.div({
  alignItems: 'center',
  color: '#7A7A7A',
  display: 'flex',
  flexWrap: 'wrap',
  fontFamily: "'Segoe UI'",
  fontSize: 16,
  width: '100%' //IE
}, ({ fonts={} }) => ({
  ...fonts,
}));

const Separator = glamorous.span({
  color: '#0297C9',
  fontSize: '10px !important',
  margin: 5
});

const Entry = glamorous(Item)({
  flexDirection: 'row'
})

const Subtitle = ({ fonts, entries }) => {
  const nEntries = R.length(entries);

  const entriesToDOM = R.addIndex(R.reduce)(
    (memo, entry, index) => R.pipe(
        R.append(
          <Entry key={`entry${index}`} {...entry} />
        ),
        R.when(
          R.always(index < nEntries - 1),
          R.append(
            <Separator
              className={`${Classes.ICON_STANDARD} ${Classes.iconClass('symbol-circle')}`}
              key={`separator${index}`}
            />
          )
        )
    )(memo),
    [],
    entries
  );

  return (
    <Container
      fonts={fonts}
    >
      { entriesToDOM }
    </Container>
  );
};

Subtitle.propTypes = {
  fonts: PropTypes.object,
  entries: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    flags: PropTypes.array,
  })),
};

export default Subtitle;
