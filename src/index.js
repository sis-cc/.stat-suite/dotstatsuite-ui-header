import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import '@blueprintjs/core/dist/blueprint.css';
import * as R from 'ramda';
import Item from './item';
import Subtitle from './subtitle';

const Container = glamorous.div({
  alignItems: 'baseline',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  marginBottom: 10,
  marginTop: 10,
});

const Title = glamorous.div({
  color: '#0297C9',
  width: '100%', //IE
  fontSize: 24,
  fontFamily: "'Segoe UI'"
}, ({ fonts={} }) => ({
  ...fonts
}));

const Uprs = glamorous.div({
  color: '#7A7A7A',
  fontFamily: "'Segoe UI'",
  fontSize: 16,
  width: '100%' //IE
}, ({ fonts={} }) => ({
  ...fonts
}));

const Disclaimer = glamorous.div({
  color: '#f7a32c',
  fontFamily: "'Segoe UI'",
  fontSize: 16,
  width: '100%' //IE
}, ({ fonts={} }) => ({
  ...fonts
}));

const Header = ({ disclaimer, fonts, subtitle, title, uprs }) => {
  return (
    <Container>
      <Title fonts={R.propOr({}, 'title', fonts)}><Item {...title} /></Title>
      <Subtitle entries={subtitle || []} fonts={R.propOr({}, 'subtitle', fonts)} />
      { R.not(R.isNil(uprs)) && <Uprs tabIndex={0} fonts={R.propOr({}, 'uprs', fonts)}>{uprs}</Uprs>}
      { R.not(R.isNil(disclaimer)) && <Disclaimer tabIndex={0} fonts={R.propOr({}, 'disclaimer', fonts)}>{disclaimer}</Disclaimer> }
    </Container>
  );
};

Header.propTypes = {
  disclaimer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  fonts: PropTypes.object,
  subtitle: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    flags: PropTypes.array,
  })),
  title: PropTypes.shape({
    label: PropTypes.string,
    flags: PropTypes.array,
  }),
  uprs: PropTypes.string
};

export default Header;
