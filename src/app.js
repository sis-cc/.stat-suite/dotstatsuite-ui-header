import React from 'react';
import { render } from 'react-dom';
import { Button } from '@blueprintjs/core';
import html2canvas from 'html2canvas';
import FileSaver from 'file-saver';
import Header from '.';

const title = { label: 'Key Short-Term Economic Indicators', flags: [{ code: 'A', label: 'title flag 1' }, { label: 'title flag 2' }] };
const subtitle = [
  { label: 'Measure: Growth previous period' },
  { label: 'Frequency: Annual', flags: [{ label: 'subtitle value flag' }] },
  { label: 'Banknote & coin related items: Collector coins - Collector coins issued less collector coins returned; not intended for circulation; (net issuance)' },
  { label: 'BKN denomination breakdown: All denominations' },
  { label: 'Banknote/coin series: Not applicable' },
  { label: 'Series denominat/spec calcul: National currency' },
  { label: 'population: TOTAL POPULATION'},
];
const uprs = 'Percentage';

const disclaimer = `This is a disclaimer about the data you're currently viewing`;

const fonts = {
  subtitle: {
    color: 'red',
    fontFamily: "'Comic sans MS'"
  },
  title: {
    color: 'green',
    fontFamily: "'Comic sans MS'",
  },
  uprs: {
    color: 'purple',
    fontFamily: "'Comic sans MS'"
  },
  tooltip: {
    fontFamily: "'Comic sans MS'",
    backgroundColor: 'yellow',
    color: 'blue'
  }
}

const download = ({ id = 'header0'} = {}) => {
  let options = { scale: 2, scrollY: -window.scrollY };

  html2canvas(document.getElementById(id), options)
    .then(function(canvas) {
      canvas.toBlob(function(blob) {
        FileSaver.saveAs(blob, `${id}.png`);
      });
    })
};

const style = {
  border: '1px solid black',
  margin: 12,
  padding: 12,
};

const App = () => (
  <div>
    <button onClick={() => download()}>download first</button>
    <button onClick={() => download({ id: 'headern' })}>download last</button>
    <div id="header0" style={style}>
      <Header disclaimer={disclaimer} uprs={uprs} subtitle={subtitle} title={title} />
    </div>
    <div style={style}>
      <Header disclaimer={disclaimer} uprs={uprs} subtitle={subtitle} />
    </div>
    <div style={style}>
      <Header disclaimer={disclaimer} uprs={uprs} title={title} />
    </div>
    <div style={style}>
      <Header disclaimer={disclaimer} subtitle={subtitle} title={title} />
    </div>
    <div style={style}>
      <Header uprs={uprs} subtitle={subtitle} title={title} />
    </div>
    <div id="headern" style={style}>
      <Header uprs={uprs} subtitle={subtitle} title={title} fonts={fonts} />
    </div>
  </div>
);

render(<App />, document.getElementById('root'));