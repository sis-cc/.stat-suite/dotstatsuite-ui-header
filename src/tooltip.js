import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import * as R from 'ramda';
import { Icon, Position, Tooltip } from '@blueprintjs/core';

const StyledFlags = glamorous.div({
  display: 'flex',
  flex: 'row',
  alignItems: 'baseline',
  marginLeft: 4,
  fontSize: 12
});

const Content = glamorous.ul({
  borderRadius: 3,
  boxSizing: 'border-box',
  color: 'white',
  fontFamily: "'Segoe UI'",
  fontSize: 12,
  listStyleType: 'none',
  maxWidth: 270,
  padding: 0,
});

const StyledTooltip = glamorous(Tooltip)({
  cursor: 'default',
  position: 'unset !important',
});

const MyTooltip = ({ flags }) => {
  if (R.isNil(flags) || R.isEmpty(flags)) return null;

  const uncodedIcon = <Icon iconName="asterisk" iconSize={null} />;

  const codedIcons = R.pipe(
    R.reduce(
      (acc, flag) => R.when(
        R.always(R.has('code', flag)),
        R.append(flag.code)
      )(acc),
      []
    ),
    R.join(',')
  )(flags);

  const hasUncoded = R.any(R.complement(R.has('code')), flags);

  const content = (
    <Content>
      {R.addIndex(R.map)(
        ({ label }, index) => <li key={index}>{label}</li>,
        flags
      )}
    </Content>
  );

  return (
    <StyledTooltip
      content={content}
      hoverCloseDelay={0}
      hoverOpenDelay={0}
      transitionDuration={10}
      position={Position.RIGHT}
      useSmartPositioning={true}
    >
      <StyledFlags>
        ({codedIcons}{!R.isEmpty(codedIcons) && hasUncoded ? ',' : null}{hasUncoded ? uncodedIcon : null})
      </StyledFlags>
    </StyledTooltip>
  );
};

MyTooltip.propTypes = {
  flags: PropTypes.arrayOf(PropTypes.shape({
    code: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    label: PropTypes.string
  })),
};

export default MyTooltip;
