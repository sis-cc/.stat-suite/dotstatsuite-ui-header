import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import glamorous from 'glamorous';
import Tooltip from './tooltip';

const Container = glamorous.div({
  display: 'flex',
  alignItems: 'center'
});

const Item = ({ label, flags }) => {
  const hasNoFlags = R.anyPass([R.isNil, R.isEmpty])(flags);
  if (R.isNil(label)) return null;

  return (
    <Container>
      <div tabIndex={0}>{label}</div>
      {
        hasNoFlags
        ? null
        : <Tooltip flags={flags} />
      }
    </Container>
  );
};

Item.propTypes = {
  label: PropTypes.string,
  flags: PropTypes.array,
};

export default Item;
